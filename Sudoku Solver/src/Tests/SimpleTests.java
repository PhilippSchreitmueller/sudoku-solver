package Tests;


import SudokuLib.SudokuSolver;
import SudokuLib.SudokuState;

public class SimpleTests {
	public static void main(String[] args){
		SudokuState s1 = new SudokuState();
		System.out.println("To solve!");
		int[][] field= new int[9][9];
		field[0][0]=9;
		field[0][1]=1;
		s1.setState(field);
		System.out.println(s1);
		
		SudokuState solved1 = SudokuSolver.solveSudoku(s1);
		if(solved1!=null)
		System.out.println(solved1);
		else
			System.out.println("No Solution found!");
		
	}
}

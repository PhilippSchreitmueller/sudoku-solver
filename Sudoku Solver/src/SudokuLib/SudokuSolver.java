package SudokuLib;

import java.util.LinkedList;

public class SudokuSolver {
	static LinkedList<SudokuState> open;
	static LinkedList<SudokuState> closed;

	public SudokuSolver() {
	
	}

	public static SudokuState solveSudoku(SudokuState start) {
		if(!start.isCurrentlyValid())
			return null;
		open = new LinkedList<SudokuState>();
		closed = new LinkedList<SudokuState>();
		System.out.println("Solving in progress...");
		open.add(start);
		LinkedList<SudokuState> temp;
		int currentBest = 0;
		while (!open.isEmpty()) {
			SudokuState x = open.remove();
			temp = new LinkedList<SudokuState>();
			if (x.getCount() > currentBest)
				currentBest = x.getCount();
			System.out
					.println("Current count: " + x.getCount()
							+ "| Open.size()= " + open.size() + "|Best: "
							+ currentBest);
			if (x.isFull()) {
				System.out.println("SUCCESS!");
				return x;
			} else {
				closed.add(x);
				for (SudokuState y : x.nextState()) {
					if (!open.contains(y) && !closed.contains(y)) {
						temp.addLast(y);
					}
				}
				open.addAll(0, temp);
			}
		}
		return null;
	}

}

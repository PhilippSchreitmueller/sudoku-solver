package SudokuLib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SudokuState {
	/**
	 * Represents the sudoku
	 */
	int[][] field;
	/**
	 * Next position to be filled
	 */
	private int counter;

	public SudokuState() {
		field = new int[9][9];
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				field[i][j] = 0;
		counter = 0;
	}
	/**
	 * Sets the sudoku state
	 * @param newField The sudoku matrix in a 2-dimensional array
	 */
	public void setState(int[][] newField) {
		if (newField[0].length != 9 || newField.length != 9)
			return;
		this.counter = 0;
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++) {
				this.field[i][j] = newField[i][j];
			}
	}
	
	/**
	 * Delivers the set of following states. The method fills in a new number at the
	 * position counter points to.
	 * @return
	 */
	public List<SudokuState> nextState() {
		List<SudokuState> ret = new ArrayList<SudokuState>();
		for (int i = 1; i <= 9; i++) {
			SudokuState s = new SudokuState();
			while (field[counter / 9][counter % 9] != 0)
				counter = counter + 1 % 81;
			s = this.insertNewNumber(counter % 9, counter / 9, i);
			if (s != null)
				ret.add(s);
		}
		return ret;
	}

	private SudokuState insertNewNumber(int x, int y, int number) {
		if (checkValidity(x, y, number)) {
			SudokuState ret = new SudokuState();
			ret.setField(this.field);
			ret.field[y][x] = number;
			ret.setCounter(this.counter + 1);
			return ret;
		}
		return null;
	}

	private boolean checkValidity(int x, int y, int number) {
		// check row
		for (int i = 0; i < 9; i++) {
			// row
			if (this.field[y][i] == number)
				return false;
			// column
			if (this.field[i][x] == number)
				return false;
			// square
			int quadx = 0;
			int quady = 0;
			int nextPos = getCount();// +1;
			if (nextPos % 9 <= 2)
				quadx = 0;
			else if (nextPos % 9 <= 5)
				quadx = 3;
			else
				quadx = 6;
			if (nextPos <= 26)
				quady = 0;
			else if (nextPos <= 53)
				quady = 3;
			else
				quady = 6;
			if (this.field[quady + ((i + 1) / 3) % 3][(i + 1) % 3 + quadx] == number)
				return false;
		}
		return true;
	}

	private void setField(int[][] original) {
		for (int i = 0; i < original.length; i++) {
			field[i] = Arrays.copyOf(original[i], original[i].length);
		}
	}

	private void setCounter(int newCount) {
		this.counter = newCount;

	}

	@Override
	public String toString() {
		String ret = "";
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				ret += field[i][j] + " ";
				if ((j + 1) % 3 == 0)
					ret += " | ";
			}
			ret += "\n";
			if ((i + 1) % 3 == 0) {
				for (int y = 0; y < 13; y++)
					ret += "--";
				ret += "\n";
			}
		}
		return ret;
	}
	/**
	 * Check if sudoku is completely filled
	 * @return true if finished
	 */
	public boolean isFull() {
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				if (field[i][j] == 0)
					return false;
		return true;
	}
	/**
	 * Get current counter position.
	 * @return
	 */
	public int getCount() {
		return counter;
	}

	@Override
	public boolean equals(Object s) {
		SudokuState arg0 = (SudokuState) s;
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++)
				if (this.field[i][j] != arg0.field[i][j])
					return false;
		return true;
	}
	
	/**
	 * Checks if the sudoku is correctly filled.
	 */
	public boolean isCurrentlyValid() {
		for (int i = 0; i < 9; i++)
			for (int j = 0; j < 9; j++) {
				int x = field[i][j];
				if (x != 0) {
					field[i][j] = 0;
					if (!checkValidity(j, i, x))
						return false;
					else
						field[i][j]=x;
				}
			}
		return true;
	}
}

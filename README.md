# Sudoku Solver #

2 classes to solve any Sudoku using DFS.

##SudokuLib##

* SudokuState: Datastructure for a sudoku. Methods to provide next states and check if full/valid.

* SodukuSolver: Provides the DFS search using two queues.*